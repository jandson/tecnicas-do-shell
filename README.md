# Técnicas do Shell

Repositório de recursos do curso Técnicas do Shell

Embora as aulas ao vivo sejam exclusivas para alunos inscritos e apoiadores com acesso promocional, todo recurso desenvolvido durante o curso será publicado livre e gratuitamente neste repositório.

* [Inscrições e renovações de acesso](https://blauaraujo.com/2022/03/12/curso-permanente-tecnicas-do-shell/)
* [Dúvidas sobre os tópicos do curso (issues)](https://codeberg.org/blau_araujo/tecnicas-do-shell/issues)

Discussões em tempo real:

[![Gitter](https://badges.gitter.im/blau_araujo/community.svg)](https://gitter.im/blau_araujo/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)