# Vídeos e textos recomendados

## Vídeos

### Para aprender o essencial do shell

- [Intensivão de programação do Bash (59 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGr53w4IzUzbPCqR4HPOHjAI)
- [Curso básico de programação do Bash (19 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGpf4X-NdGjvSlEFZhn2f2H7)

### Aprofundamento

- [Curso Shell GNU (19 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGqJEEyo2fY3SA-QCKlF2rxO)

### Playlists "mão na massa" e papos de shell

- [Sextas Shell (8 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGouK99nsHs-4CUjr21QTAuy)
- [Criação de scripts em Bash (15 vídeos)](https://youtube.com/playlist?list=PLXoSGejyuQGrjEIS_tIJ7XYJTcc1ggQy-)


## Textos

### Para aprender o essencial

- [Apostilas do Curso Básico de Programação em Bash](https://debxp.org/cbpb/)
- [Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)

### Aprofundamento

- [Apostilas do Curso Shell GNU](https://blauaraujo.com/curso-shell-gnu/)
- [Expressões regulares no shell](https://blauaraujo.com/shell/livro/regex-shell)

### Artigos

1. [O problema do falso negativo](https://debxp.org/bash-o-problema-do-falso-negativo/)
1. [Identificar e listar arquivos duplicados](https://debxp.org/shell-identificar-e-listar-arquivos-duplicados/)
1. [Instalando listas de pacotes em arquivos (Bash+Apt)](https://debxp.org/debian-instalando-listas-de-pacotes-em-arquivos/)
1. [Anotações do Curso Básico de Programação do Shell](https://debxp.org/anotacoes-do-curso-basico-de-programacao-do-shell-aula-1/)
1. [Duplicação de descritores de arquivos](https://debxp.org/duplicacao-de-descritores-de-arquivos/)
1. [As regras ocultas do `getopts`](https://debxp.org/as-regras-ocultas-do-getopts/)
1. [Definindo as cores do TTY](https://debxp.org/definindo-as-cores-do-tty/)
1. [E agora, quem poderá nos proteger?](https://debxp.org/e-agora-quem-podera-nos-proteger/)
1. [Deu a louca no "test" do Bash](https://debxp.org/deu-a-louca-no-test-do-bash/)
1. [A história do `grep`](https://debxp.org/a-historia-do-grep/)
1. [Anotações do Curso Shell Prático](https://debxp.org/anotacoes-do-curso-shell-pratico-aula-1/)
1. [Dialogando com o `dialog`](https://debxp.org/dialogando-com-o-dialog/)
1. [Criação de arquivos vazios no shell](https://debxp.org/criacao-de-arquivos-vazios-no-shell/)
1. [Não olhe para trás, `grep`!](https://blauaraujo.com/shell/livro/desafios/grep-nao-olhe-para-tras)
1. [Explodindo strings com `BASH_REMATCH`](https://debxp.org/explodindo-strings-com-bash_rematch/)
1. [Casamento de padrões com “globs” estendidos](https://debxp.org/casamento-de-padroes-com-globs-estendidos/)
1. [Globbing e as variáveis 'LANG' e 'LC_'](https://debxp.org/globbing-e-as-variaveis-lang-e-lc_/)

